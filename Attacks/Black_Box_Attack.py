import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np

from art.attacks.evasion import HopSkipJump, SimBA, BoundaryAttack
from art.attacks.poisoning import *


class Black_Box_Attack():
    def __init__(self, Classifier, x_test, y_test):
        self.Classifier = Classifier
        self.x_test = x_test
        self.y_test = y_test

    def Create_Attack(self, att):
        
        if(att == "HSJ"):

            print("Applying HopSkipJump Attack".center(35, ">"),"\n")
            attack = HopSkipJump(classifier=self.Classifier, targeted=False,
                                 max_iter=10, max_eval=1000, init_eval=10)
            x_test_adv = attack.generate(x=self.x_test)

        elif(att == "SBA"):

            print("Applying SimBA Attack".center(29, ">"),"\n")
            attack = SimBA(classifier=self.Classifier, attack="px",
                           epsilon=0.2, targeted=False)
            x_test_adv = attack.generate(x=self.x_test)

        elif(att == "BA"):

            print("Applying BoundaryAttack".center(31, ">"),"\n")
            attack = BoundaryAttack(
                estimator=self.Classifier, epsilon=0.02, verbose=False)
            x_test_adv = attack.generate(x=self.x_test, y=self.y_test)

        return x_test_adv, attack
